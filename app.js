'use strict';

$(document).ready(function()  {
    const ALL = 0;
    const ONLY_COMPLETED = 1;
    const ONLY_ACTIVE = 2;
    var currentStatus = ALL;
    var currentPage = 1;
    var myArray = [];





    render();

    // $( "#form-todo" ).on( "keydown", function(event) {
    //     if (event.which == 13) {
    //         $(this).submit();
    //     }
    // });
    // $("#submit").on("click", addElement);
    $("#form-todo").on("submit", function addElement(e) {
        e.preventDefault();
        let newElement = $('#new-todo').val().trim();
        if (newElement != ""){
            let a = myArray.length - 1;
            if (myArray == ''){
                a = 0;
            }
            else{
                a = myArray[a].id + 1;
            }
            myArray.push({
                id:a,
                done:false,
                text:newElement
            });
            $('#new-todo').val(null);

            currentPage = 1;
            render();
        }
        return false;
    });
    function render() {

        var k = 0;
        var html = "";
        var completed = '';
        var j;
        var numberOfCompleted = 0;
        counteCompleted (numberOfCompleted);
        fixPageNumber();

        // if (currentStatus == ALL)
        // {
            j = myArray.length - 1 - 10*(currentPage - 1);
        // }

        // myArray.forEach(function(item) {
        //     // console.log('--->',item,neededElement)
        //     if(item.id == neededElement) {
        //         console.log(item,myArray);
        //         item.done = true;
        //     }
        // })
        // else if (currentStatus == ONLY_COMPLETED)
        // {
        //     j =  numberOfCompleted - 1 - 10*(currentPage - 1);
        // }
        // else if (currentStatus == ONLY_ACTIVE)
        // {
        //     j = myArray.length - numberOfCompleted - 1 - 10*(currentPage - 1);
        // }
        while ((k != 10) && (j >= 0)) {
            if ((!(((currentStatus == ONLY_ACTIVE) && (myArray[j].done == true))
                || ((currentStatus == ONLY_COMPLETED) && (myArray[j].done == false))))
                && (myArray != "")) {

                    if ( myArray.done == true) {
                        completed = 'checked';
                    }
                    else{
                        completed = '';
                    }
                // }
                html += '<li class="todo" id="'+ j +'">'+
                        '<input class="checkbox" type="checkbox"'+ completed +'/>'+
                        '<input class="text '+ completed +'" value="'+ myArray[j].text +'" readonly/>'+
                        '<button class="delete">x</button>'+
                        '</li>';
                // console.log(k);
                k++;
            }
            // else {
            //
            // }
            j--;
        };


        let counter = 'Completed : ' + numberOfCompleted + ' / Active : ' + (myArray.length - numberOfCompleted);
        $('#completed-counter').text(counter);

        $('#todo-list').html(html);
        pagination(numberOfCompleted);
    };

    $("#todo-list").on('click',".checkbox", function () {
        var neededElement = $(this).parent().attr('id');
        // var result = $.grep(myArray, function(e){ return e.id == id; });
        // console.log(neededElement);
        // var result = $.grep(myArray, function(e){ return e.id == id; });
        if ($(this).is(':checked')) {

            myArray.forEach(function(item) {
                // console.log('--->',item,neededElement)
                if(item.id == neededElement) {
                    console.log(item,myArray);
                    item.done = true;
                }
            })
        }
        else{
            // console.log(1);
            myArray.forEach(function(item) {
                // console.log('--->',item,neededElement)
                if(item.id == neededElement) {
                    console.log(item);
                    item.done = false;
                }
            })
        }
        render();

    });

    $("#todo-list").on('dblclick','.text',function () {
        $(this).attr('readonly', false);
    });

    $("#todo-list").on('focusout','.text', function ElementChange() {
        if ($(this).attr('readonly', false)) {
            $(this).attr('readonly', true);
            var neededElement = $(this).parent().attr('id');
            // arr.forEach((myArray){

            $.each( myArray, function( ) {
                if(myArray.id == neededElement) {
                    myArray.text = $(this).val().trim();
                    if (myArray.text == '' ){
                        myArray.splice(1);
                    }
                }

            });
            // });



            render();

        }
    });

    $("#todo-list").on('click', '.delete', function(){
        var neededElement = $(this).parent().attr('id');
        myArray.splice(neededElement,1);
        // console.log($(this).parent().index());

        for (let i = neededElement; i < myArray.length; i++){
            myArray[i].id = i;
        }

        render();
    });

    $("#delete-completed").on('click', function () {
        // let m = myArray.length;
        // console.log(m + ' m');
        for (let j = myArray.length - 1; j >= 0; j--) {
            // console.log(j + ' j');
            // console.log(myArray);
            if (myArray[j].done == true){
                myArray.splice(j,1);
                alert(j);
                // numberOfCompleted--;
                // j--;
            }
            // console.log(myArray);
        }
        // console.log(myArray);
        for (let i = 0; i < myArray.length; i++){
            myArray[i].id = i;
        }
        render();
    });

    $("#pagination").on('click', '#prev' , function(){
        if (currentPage > 1) {
            currentPage = currentPage - 1;
            render();
        }
    });

    $("#pagination").on('click','#next', function(){
        if (currentPage*10 < myArray.length) {
            currentPage = currentPage + 1;
            render();
            // console.log("втф");
        }
    });
    
    function counteCompleted (numberOfCompleted) {

        numberOfCompleted = 0;
        for (let i = 0; i < myArray.length; i++){
            if (myArray[i].done == true){
                numberOfCompleted++;
            }
        }
        return(numberOfCompleted);
        // console.log(numberOfCompleted);
    };

    $("#all-completed").on('click', function () {
        if ($(this).is(':checked')) {
            for (let i = 0; i < myArray.length; i++) {
                myArray[i].done = true;
            }
        }
        else{
            for (let i = 0; i < myArray.length; i++) {
                myArray[i].done = true;
            }
        }
        render();
    });

    function fixPageNumber(){
        while (myArray.length <= 10*(currentPage-1)){
            // console.log(myArray.length);
            // console.log(currentPage);
            currentPage--;

        }
    };

    $('#show-all').on('click', function () {
        if (currentStatus != ALL){
            currentStatus = ALL;
            render();
        }
    });

    $('#show-active').on('click', function () {
        if (currentStatus != ONLY_ACTIVE){
            currentStatus = ONLY_ACTIVE;
            render();
        }
    });

    $('#show-completed').on('click', function () {
        if (currentStatus != ONLY_COMPLETED){
            currentStatus = ONLY_COMPLETED;
            render();
        }
    });

    function pagination(numberOfCompleted) {
        var html = '';
        var totalPages;
        if (currentStatus == ALL){
            totalPages = Math.floor((myArray.length - 1)/10) + 1;
        }
        if (currentStatus == ONLY_ACTIVE){
            totalPages = Math.floor((myArray.length - numberOfCompleted - 1)/10) + 1;
        }
        if (currentStatus == ONLY_COMPLETED){
            totalPages = Math.floor((numberOfCompleted - 1)/10) + 1;
        }


        // console.log(totalPages);
        // console.log(currentPage);

        if (totalPages > 1) {
            html += '<span id="prev" class = "pages"> << </span>';
            if (currentPage != 1){
                html += '<span class = "pages number-page"> 1 </span>';
            }

            if (currentPage > 4){
                html += '<span id="more" class = "pages"> ... </span>';
            }
            if (currentPage - 2 > 1){
                html += '<span class = "pages number-page">' + (currentPage - 2) + '</span>';
            }
            if (currentPage - 1 > 1){
                html += '<span class = "pages number-page">' + (currentPage - 1) + '</span>';
            }
            // if (currentPage != totalPages){
                html += '<span class = "pages number-page" id="currentPage">' + currentPage + '</span>';
            // }
            if (currentPage + 1 < totalPages){
                html += '<span class = "pages number-page">' + (currentPage + 1) + '</span>';
            }
            if (currentPage + 2 < totalPages){
                html += '<span class = "pages number-page">' + (currentPage + 2) + '</span>';
            }
            if (currentPage  < totalPages - 3){
                html += '<span id="more" class = "pages"> ... </span>';
            }
            if (currentPage != totalPages){
                html += '<span class = "pages number-page">' + totalPages + '</span>';
            }

            html += '<span id="next" class = "pages"> >> </span>';
        }
        $('#pagination').html(html);

    };

    $("#pagination").on('click','.number-page', function(){
        console.log($(this).text());
        var c = $(this).text();
        currentPage = c;
        console.log(c);
        render();
    });


});